package ru.bokhan.tm.exception.empty;

public final class EmptyUserIdException extends RuntimeException {

    public EmptyUserIdException() {
        super("Error! User ID is empty...");
    }

}