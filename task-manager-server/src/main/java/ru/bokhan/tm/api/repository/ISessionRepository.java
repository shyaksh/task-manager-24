package ru.bokhan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.api.IRepository;
import ru.bokhan.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    List<Session> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

}